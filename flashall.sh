#!/bin/bash
set -e
logo(){
	clear
	cat << "EOF"

			   ++ 
     =================== ++++++
     =        ====         ++
     =          ==       
     =          ==          =
     =          ==          =
     =          ==          =
     =          ==          = 
     =          ==          =
     =        ======        = 
     ========================
  ##########################################
  # PEROM Installer For OnePlus8/op8pro/8t #
  #                                        #
  # By Broly @Broly_1 telegram             #
  ##########################################

EOF
}
flashall(){
	logo
	fastboot flash recovery recovery.img
	logo
	fastboot flash boot boot.img
	logo
	fastboot flash dtbo dtbo.img
	logo
	fastboot reboot fastboot
	logo
	fastboot flash odm odm.img
	logo
	fastboot flash system system.img
	logo
	fastboot flash system_ext system_ext.img
	logo
	fastboot flash product product.img
	logo
	fastboot flash vbmeta vbmeta.img
	logo
	fastboot flash vbmeta_system vbmeta_system.img
	logo
	fastboot flash vendor vendor.img
	logo
	printf "CLICK ENTER RECOVERY FACTORY RESET AND REBOOT"

	cat << "EOF"

 #########################################
 #             ONEPLUS LOVE!             #
 #########################################

EOF
}
logo
while true; do
	read -r -p "$(printf %s "Do you want to flash this Rom (y/n)? ")" yn
	case $yn in
		[Yy]* ) flashall; break;;
		[Nn]* ) exit;;
		* ) printf "Please answer yes or no.\n";;
	esac
done
